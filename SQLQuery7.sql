CREATE DATABASE MockingBank;

GO
/****** Object:  Table [dbo].[Customer]    Script Date: 12/07/2019 10:01:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Customer](
	[CustomerID] [uniqueidentifier] NOT NULL,
	[CustomerFirstName] [nvarchar](50) NULL,
	[CustomerLastName] [nvarchar](50) NULL,
	[CardNumber] [nvarchar](50) NULL,
	[ExpiryDate] [nvarchar](50) NULL,
	[CVCode] [nvarchar](50) NULL,
	[Balance] [decimal](18, 0) NULL,
 CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED 
(
	[CustomerID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ELMAH_Error]    Script Date: 12/07/2019 10:01:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ELMAH_Error](
	[ErrorId] [uniqueidentifier] NOT NULL,
	[Application] [nvarchar](60) NULL,
	[Host] [nvarchar](50) NULL,
	[Type] [nvarchar](100) NULL,
	[Source] [nvarchar](60) NULL,
	[Message] [nvarchar](500) NULL,
	[User] [nvarchar](50) NULL,
	[StatusCode] [int] NULL,
	[TimeUtc] [datetime] NULL,
	[Sequence] [int] IDENTITY(1,1) NOT NULL,
	[AllXml] [ntext] NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Transaction]    Script Date: 12/07/2019 10:01:55 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Transaction](
	[TransactionID] [uniqueidentifier] NOT NULL,
	[CustomerID] [uniqueidentifier] NOT NULL,
	[TransactionAmount] [nvarchar](50) NULL,
	[TransactionDetails] [nvarchar](50) NULL,
	[TransactionDate] [nvarchar](50) NULL,
	[Status] [nvarchar](50) NULL,
 CONSTRAINT [PK_Transaction] PRIMARY KEY CLUSTERED 
(
	[TransactionID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[Customer] ([CustomerID], [CustomerFirstName], [CustomerLastName], [CardNumber], [ExpiryDate], [CVCode], [Balance]) VALUES (N'b7f0fd04-59f0-41bc-8576-11d9f62b2ecb', N'Krishna', N'Nundun', N'123456789123', N'10/20', N'123', CAST(4000 AS Decimal(18, 0)))
GO
INSERT [dbo].[Transaction] ([TransactionID], [CustomerID], [TransactionAmount], [TransactionDetails], [TransactionDate], [Status]) VALUES (N'9b49237d-aec1-4fdd-9107-11de1e34ddae', N'b7f0fd04-59f0-41bc-8576-11d9f62b2ecb', N'200', N'Buy Package', N'07-10-2019', N'Completed')
GO
INSERT [dbo].[Transaction] ([TransactionID], [CustomerID], [TransactionAmount], [TransactionDetails], [TransactionDate], [Status]) VALUES (N'24cc9a41-e956-4ab8-a7e4-b82e9b084624', N'b7f0fd04-59f0-41bc-8576-11d9f62b2ecb', N'300', N'Buy Internet Package', N'04-10-2019', N'Completed')
GO
/****** Object:  Index [PK_ELMAH_Error]    Script Date: 12/07/2019 10:01:55 ******/
ALTER TABLE [dbo].[ELMAH_Error] ADD  CONSTRAINT [PK_ELMAH_Error] PRIMARY KEY NONCLUSTERED 
(
	[ErrorId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, FILLFACTOR = 90) ON [PRIMARY]
GO
ALTER TABLE [dbo].[ELMAH_Error] ADD  CONSTRAINT [DF_ELMAH_Error_ErrorId]  DEFAULT (newid()) FOR [ErrorId]
GO
ALTER TABLE [dbo].[Transaction]  WITH CHECK ADD  CONSTRAINT [FK_Transaction_Customer] FOREIGN KEY([CustomerID])
REFERENCES [dbo].[Customer] ([CustomerID])
ON UPDATE CASCADE
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Transaction] CHECK CONSTRAINT [FK_Transaction_Customer]
GO
