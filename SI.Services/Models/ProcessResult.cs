﻿namespace SI.Services.Models
{
    public class ProcessResult
    {
        public bool Status { get; set; }
        public int StatusCode { get; set; }
    }
}
