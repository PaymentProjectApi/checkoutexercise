﻿namespace SI.Services.Models
{
    public static class MessageResponses
    {
        public static string GetMessage(int StatusCode)
        {
            var message = string.Empty;
            switch (StatusCode)
            {
                // 9000 Payment Successful
                case 9000:
                    message = "Payment Successful";
                    break;
                // 9001 Payment Declined
                case 9001:
                    message = "Payment Declined";
                    break;
                default:
                    message = "";
                    break;
            }
            return message;
        }
    }
}
