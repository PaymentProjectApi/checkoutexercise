﻿using System;
using System.Collections.Generic;

namespace SI.Services.Models
{
    public class CustomerDetailsList
    {
        public List<CustomerDetailsListItems> Items { get; } = new List<CustomerDetailsListItems>();
    }

    public class CustomerDetailsListItems
    {
        public Guid CustomerID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CardNumber { get; set; }
        public string ExpiryDate { get; set; }
        public string CVV { get; set; }
        public TransactionDetailsList TransactionDetails { get; set; }
    }
}
