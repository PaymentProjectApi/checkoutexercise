﻿using SI.Services.Models;
using System;

namespace SI.Services.Interfaces
{
    public interface ICustomerService
    {
        CustomerDetailsList GetListofCustomer();
        CustomerDetailsList GetSpecificCustomerDetails(Guid CustomerID);
    }
}
