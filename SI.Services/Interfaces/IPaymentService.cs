﻿using SI.Services.Models;
using System;

namespace SI.Services.Interfaces
{
    public interface IPaymentService
    {
        ProcessResult AddTransaction(Guid customerID,string FName, string LName, string cardNumber,string expiry,string cvv, decimal balance);
    }
}
