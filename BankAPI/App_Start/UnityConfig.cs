using Infrastructure.Repositories;
using SI.Services.Interfaces;
using System.Web.Http;
using Unity;
using Unity.WebApi;

namespace BankAPI
{
    public static class UnityConfig
    {
        public static IUnityContainer Container { get; set; }
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();
            container.RegisterType<ICustomerService, CustomerRepository>();
            container.RegisterType<IPaymentService, PaymentRepository>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}