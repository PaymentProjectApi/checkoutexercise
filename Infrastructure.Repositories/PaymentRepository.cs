﻿using Domain.Entities;
using SI.Services.Interfaces;
using SI.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class PaymentRepository : IPaymentService
    {
        Repository<Customer> customerdb = new Repository<Customer>();
        Repository<Transaction> transactiondb = new Repository<Transaction>();

        public ProcessResult AddTransaction(Guid customerID, string FName, string LName, string cardNumber, string expiry, string cvv, decimal balance)
        {
            var checkEmployee = customerdb.FindBy(i => i.Id == customerID && i.CustomerFirstName == FName &&
            i.CustomerLastName == LName && i.CardNumber == cardNumber && i.ExpiryDate == expiry && i.CVCode == cvv).Select(i => i).Any();

            if (checkEmployee)
            {
                var checkBalance = customerdb.FindBy(i => i.Id == customerID).Where(i => i.Balance > balance).Any();

                if (checkBalance)
                    return new ProcessResult { Status = true, StatusCode = 9000 };
                else
                    return new ProcessResult { Status = false, StatusCode = 9001 };
            }
            else
                return new ProcessResult { Status = false, StatusCode = 9001 };


        }
    }
}
