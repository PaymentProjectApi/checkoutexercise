﻿using Domain.Entities;
using SI.Services.Interfaces;
using SI.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repositories
{
    public class CustomerRepository : ICustomerService
    {
        //private BankContext _db;

        //public CustomerRepository(BankContext db)
        //{
        //    _db = db;
        //}
        Repository<Customer> customerdb = new Repository<Customer>();
        Repository<Transaction> transactiondb = new Repository<Transaction>();

        public CustomerDetailsList GetListofCustomer()
        {
            var customerList = new CustomerDetailsList();

            var customerListItems = customerdb.GetAll().Select(
                                        i => new CustomerDetailsListItems
                                        {
                                            CustomerID = i.Id,
                                            FirstName = i.CustomerFirstName,
                                            LastName = i.CustomerLastName,
                                            CardNumber = i.CardNumber,
                                            ExpiryDate = i.ExpiryDate,
                                            CVV = i.CVCode,                                          
                                        }).ToList();

            
            foreach (var items in customerListItems)
            {
                var transactionDetailsList = new TransactionDetailsList();

                var transactionDetailsListItems = transactiondb.FindBy(i => i.CustomerID == items.CustomerID).Select(
                            i => new TransactionDetailsListItems
                            {
                                TransactionID = i.Id,
                                TransactionDate = i.TransactionDate,
                                TransactionAmount = i.TransactionAmount,
                                TransactionDescription = i.TransactionDetails,
                                TransactionStatus = i.Status
                            }).ToList();
                transactionDetailsList.Items.AddRange(transactionDetailsListItems);

                items.TransactionDetails = transactionDetailsList;
            }

            customerList.Items.AddRange(customerListItems);

            return customerList;
        }

        public CustomerDetailsList GetSpecificCustomerDetails(Guid CustomerID)
        {
            var customerList = new CustomerDetailsList();

            var customerListItems = customerdb.FindBy(i => i.Id == CustomerID).Select(
                                        i => new CustomerDetailsListItems
                                        {
                                            CustomerID = i.Id,
                                            FirstName = i.CustomerFirstName,
                                            LastName = i.CustomerLastName,
                                            CardNumber = i.CardNumber,
                                            ExpiryDate = i.ExpiryDate,
                                            CVV = i.CVCode,
                                            TransactionDetails = new TransactionDetailsList()
                                        }).ToList();

            foreach (var items in customerListItems)
            {
                var transactionDetailsList = new TransactionDetailsList();

                var transactionDetailsListItems = transactiondb.FindBy(i => i.CustomerID == items.CustomerID).Select(
                            i => new TransactionDetailsListItems
                            {
                                TransactionID = i.Id,
                                TransactionDate = i.TransactionDate,
                                TransactionAmount = i.TransactionAmount,
                                TransactionDescription = i.TransactionDetails,
                                TransactionStatus = i.Status
                            }).ToList();
                transactionDetailsList.Items.AddRange(transactionDetailsListItems);

                items.TransactionDetails = transactionDetailsList;
            }

            customerList.Items.AddRange(customerListItems);

            return customerList;
        }
    }
}
