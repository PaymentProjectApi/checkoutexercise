namespace Domain.Entities
{
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table("Transaction")]
    public partial class Transaction: BaseEntity
    {
        public Guid CustomerID { get; set; }

        [StringLength(50)]
        public string TransactionAmount { get; set; }

        [StringLength(50)]
        public string TransactionDetails { get; set; }

        [StringLength(50)]
        public string TransactionDate { get; set; }

        [StringLength(50)]
        public string Status { get; set; }

        public virtual Customer Customer { get; set; }
    }
}
